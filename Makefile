COMMENT =	zerotier client

GH_ACCOUNT =	zerotier
GH_PROJECT =	ZeroTierOne
GH_TAGNAME =	1.10.1

CATEGORIES =	net

HOMEPAGE =	https://github.com/zerotier/ZeroTierOne

PERMIT_PACKAGE =	Unsure
PERMIT_DISTFILES =	Unsure

USE_GMAKE =		Yes

.include <bsd.port.mk>
