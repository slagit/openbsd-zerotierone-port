# ZeroTierOne port for OpenBSD

This repository contains an OpenBSD port for building the ZeroTierOne
client for OpenBSD.

## Build Package

Building this package depends on the OpenBSD ports tree begin installed
on the build machine. On an OpenBSD machine, perform the following steps
to install the ports tree.

```console
cd /tmp
ftp https://cdn.openbsd.org/pub/OpenBSD/$(uname -r)/{ports.tar.gz,SHA256.sig}
signify -Cp /etc/signify/openbsd-$(uname -r | cut -c 1,3)-base.pub -x SHA256.sig ports.tar.gz
cd /usr
tar xzf /tmp/ports.tar.gz
```

Building the client requires the use of `gmake`. Installing a `gmake`
binary package ahead of time avoids building `gmake` and its dependencies
from source.

```console
pkg_add gmake
```

Build ZeroTierOne package

```console
cd /usr/ports/net
git clone https://gitlab.com/slagit/openbsd-zerotierone-port.git ZeroTierOne
cd ZeroTierOne
make package
```

The resulting binary package can be found in
`/usr/ports/packages/amd64/all/`


## Install Package

On the target system, ensure the package signing public key is located in
`/etc/signify/slagit-pkg.pub`. For convenience, a copy of the package
signing key is located in this repository. Run the following command to
install the signed package:

```console
pkg_add https://gitlab.com/slagit/openbsd-zerotierone-port/-/releases/v1.10.1/downloads/ZeroTierOne-1.10.1.tgz
```

Once installed, the client can be started with the `rcctl` command:

```console
rcctl start zerotier
```

Similarly, the client can be configured to start on boot:

```console
rcctl enable zerotier
```

## Releasing

To release a new version of this port, first double check the following
items:

* The `REVISION` variable should be updated. It should be removed if this
  is a new upstream version. Set to `0` if this is a packaging change with
  no upstream changes, and incremented for subsequent packaging changes.
* The version number in this README should be adjusted to match the new
  package version.

Next, tag the Git Repository for the release. The tag should be prefixed
with a `v`. Perform a build following the steps above.

Copy the package signing key to `/etc/signify/slagit-pkg.sec` and sign the
new binary package with the following command:

```console
cd /usr/ports/packages/amd64
pkg_sign -s signify2 -s /etc/signify/slagit-pkg.sec -o signed -S all
```

Attach the signed package from `/usr/ports/packages/amd64/signed/`,
and any release notes to the release. The following commands can be used
to attach the artifact to the release.

```console
export VERSION=1.10.1
export GITLAB_TOKEN=...
curl \
    --header "PRIVATE-TOKEN: $[GITLAB_TOKEN}" \
    --upload-file ZeroTierOne-${VERSION}.tgz \
    https://gitlab.com/api/v4/projects/38390137/packages/generic/ZeroTierOne/${VERSION}/ZeroTierOne-${VERSION}.tgz
curl \
    --header "PRIVATE-TOKEN: $[GITLAB_TOKEN}" \
    --data name=ZeroTierOne-${VERSION}.tgz \
    --data filepath=/ZeroTierOne-${VERSION}.tgz \
    --data link_type=package \
    --data url=https://gitlab.com/api/v4/projects/38390137/packages/generic/ZeroTierOne/${VERSION}/ZeroTierOne-${VERSION}.tgz \
    --request POST \
    https://gitlab.com/api/v4/projects/38390137/releases/v${VERSION}/assets/links
```
